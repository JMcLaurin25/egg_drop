
#ifndef FLOOR_H
#define FLOOR_H

#include "egg.h"

struct crate {
	egg **eggs;
	int egg_count;
};

struct crate *make_crate(int num_eggs);
void find_floor(size_t floor, struct crate *carton);

void clean_hen_house(struct crate *carton);


#endif
