
#include <stdio.h>

#include "egg.h"
#include "floor.h"

int main(int argc, char **argv)
{
	if (argc != 3) {
		printf("\n\t\t\t -- INSUFFICIENT ARGUMENTS --\n\n");
		printf("  NAME\n\tegg_drop\n\n");
		printf("  SYNOPSIS\n\tegg_drop [FLOOR_NUMBER] [EGG_NUMBER]\n\n");
		printf("  DESCRIPTION\n\tSimulate dropping of eggs off a building to determine the maximum \n\tsafe height in the fewest number of drops.\n\n");
		return 1;
	}

	char *endptr;
	int floors = strtod(argv[1], &endptr);
	int egg_count = strtod(argv[2], &endptr);
	if (floors < 1) {
		printf("You'll need to throw the eggs HARD from this height to break them.\n");
		return 1;
	} else if (egg_count < 1) {
		printf("\n\t\t\t -- NO EGGS TO DROP!\n\n");
		return 1;
	}		
	struct crate *carton = make_crate(egg_count);
	find_floor(floors, carton);

	clean_hen_house(carton);
}
