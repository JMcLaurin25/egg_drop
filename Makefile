
CFLAGS+=-std=c11 -D_GNU_SOURCE
CFLAGS+=-Wall -Wextra -Wpedantic
CFLAGS+=-Wwrite-strings -Wstack-usage=1024 -Wfloat-equal -Waggregate-return -Winline -fstack-usage

LDLIBS+=-lm

egg_drop: egg_drop.o egg.o floor.o 

.PHONY: clean debug profile

clean:
	-rm *.o *.su egg_drop

debug: CFLAGS+=-g
debug: egg_drop

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: egg_drop
