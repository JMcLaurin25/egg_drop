
#include <stdio.h>
#include "floor.h"

//Allocates a collection of eggs 
struct crate *make_crate(int num_eggs)
{
	struct crate *carton = malloc(sizeof(*carton));
	if (!carton) {
		return NULL;
	}

	carton->eggs = malloc(sizeof(carton->eggs) * num_eggs);
	if (!carton->eggs) {
		return NULL;
	}

	int index = 0;
	for (index = 0; index < num_eggs; index++) {
		carton->eggs[index] = lay_egg();
		 if (!carton->eggs[index]) {
			int cur_indx;
			for (cur_indx = index; cur_indx > 0; cur_indx--) {
				free(carton->eggs[cur_indx]);
			}
			free(carton->eggs);
			free(carton);
			return NULL;
		}
	}

	carton->egg_count = num_eggs;
	return carton;
}

//Searches until maximum height is derived
void find_floor(size_t floor, struct crate *carton)
{
	int upper_limit = floor;
	int safe_floor = 0;
	int guess_sz = upper_limit / 2;
	int guess = guess_sz;

	if (guess == 0) {
		++guess;
	}

	int drops = 0;

	int cur_egg = 0;
	int remaining = carton->egg_count;
	egg *e = carton->eggs[cur_egg];

	while (remaining > 0) {
		printf("Dropping egg %d from floor:\n", cur_egg + 1);
		if (remaining == 1) {
			break;
		}
		egg_drop_from_floor(e, guess);

		while (!egg_is_broken(e)) {
			safe_floor = guess;
			guess_sz = (upper_limit - safe_floor) / 2; 
			printf("  #%d safe\n", guess);
			guess = safe_floor + guess_sz;
			drops++;


			if (guess_sz == 0 && upper_limit == (int)floor) {
				egg_drop_from_floor(e, upper_limit);
				drops++;
				if (!egg_is_broken(e)) {
					printf("  #%d safe\n", upper_limit);
					printf("\n%d is the maximum safe floor found after %d drops\n", upper_limit, drops);
				} else {
					printf("\n%d is the maximum safe floor found after %d drops\n", safe_floor, drops);
				}
				return;
			} else if (safe_floor == upper_limit - 1) {
				printf("\n%d is the maximum safe floor found after %d drops\n", safe_floor, drops);
				return;
			}
			egg_drop_from_floor(e, guess);
		}

		upper_limit = guess;
		guess_sz = (upper_limit - safe_floor) / 2;
		printf("  #%d CRACK\n", guess);
		guess = safe_floor + guess_sz;

		e = carton->eggs[++cur_egg];
		remaining--;
		drops++;
		if (safe_floor == upper_limit - 1) {
			printf("\n%d is the maximum safe floor found after %d drops\n", safe_floor, drops);
			return;
		}
	}

	guess = safe_floor + 1;

	while (remaining == 1) {
		egg_drop_from_floor(e, guess);
		if (egg_is_broken(e)) {	
			printf("  #%d CRACK\n", guess);
			printf("\n%d is the maximum safe floor found after %d drops\n", safe_floor, drops + 1);
			remaining--;
		} else { //Egg Not broken
			printf("  #%d safe\n", guess);
			safe_floor = guess;
		}

		guess++;
		drops++;
	}
}

//Frees all mallocs of egg crate structures
void clean_hen_house(struct crate *carton)
{
	int index = 0;

	for (index = 0; index < carton->egg_count; index++) {
		free(carton->eggs[index]);
	}
	free(carton->eggs);
	free(carton);
}
